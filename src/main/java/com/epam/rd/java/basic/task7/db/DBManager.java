package com.epam.rd.java.basic.task7.db;

import com.epam.rd.java.basic.task7.db.entity.Team;
import com.epam.rd.java.basic.task7.db.entity.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;


public class DBManager {
	private static DBManager instance;
	Connection conn;

	public static synchronized DBManager getInstance() {
		if (instance == null) {
			try {
				instance = new DBManager();
			} catch (DBException e) {
				e.printStackTrace();
			}
		}
		return instance;
	}

	private DBManager() throws DBException {
		String user = "root";
		String password = "123456";

		try {
			conn = DriverManager.getConnection(
					String.format("jdbc:mysql://localhost/test_db?user=%s&password=%s", user, password));
		} catch (SQLException e) {
			System.out.println("SQLException: " + e.getMessage());
			System.out.println("SQLState: " + e.getSQLState());
			System.out.println("VendorError: " + e.getErrorCode());
			throw new DBException(e.getMessage(), e.getCause());
		}
	}

	public List<User> findAllUsers() throws DBException {
		return null;
	}

	public boolean insertUser(User user) throws DBException {
		return false;
	}

	public boolean deleteUsers(User... users) throws DBException {
		return false;
	}

	public User getUser(String login) throws DBException {
		return null;
	}

	public Team getTeam(String name) throws DBException {
		return null;
	}

	public List<Team> findAllTeams() throws DBException {
		return null;
	}

	public boolean insertTeam(Team team) throws DBException {
		return false;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		return false;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		return null;
	}

	public boolean deleteTeam(Team team) throws DBException {
		return false;
	}

	public boolean updateTeam(Team team) throws DBException {
		return false;
	}

}
